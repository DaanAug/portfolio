---
id: 026de60f-b84d-44a5-b22c-87bf7ee1cfc8
blueprint: page
title: Projects
template: default
updated_by: 1c59a747-49b0-40d8-9738-c0f9fe1e30dd
updated_at: 1680602694
blocks:
  -
    id: lg23er74
    title: Projecten
    collections_field: projects
    display_amount: 20
    type: collection_list_set
    enabled: true
---
This is the projects page