---
id: home
blueprint: pages
title: Home
template: home
blocks:
  -
    id: lg22svzk
    name: 'Daan Augusteijn'
    subtitle: 'Software Developer'
    type: hero_set
    enabled: true
  -
    id: lg14pejv
    title: 'Over mij'
    content: 'Ik ben Daan, een software development student met een passie voor programmeren en automatisering.  Ik ben vastbesloten om mijn programmeervaardigheden te verbeteren en te werken aan projecten die de wereld kunnen verbeteren. Ik hou van bakken en gitaar spelen om te ontspannen.'
    image: daan.png
    type: about_set
    enabled: true
  -
    id: lg16qo91
    title: Projecten
    collections_field: projects
    display_amount: 3
    type: collection_list_set
    enabled: true
  -
    id: lg16rwzx
    title: Blogs
    collections_field: blogs
    display_amount: 5
    type: collection_list_set
    enabled: true
updated_by: 1c59a747-49b0-40d8-9738-c0f9fe1e30dd
updated_at: 1680601714
---
