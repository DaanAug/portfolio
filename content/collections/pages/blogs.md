---
id: 884a5ad0-c6cc-49c7-b39e-6ff2403da59e
blueprint: page
title: Blogs
updated_by: 1c59a747-49b0-40d8-9738-c0f9fe1e30dd
updated_at: 1680602683
template: default
blocks:
  -
    id: lg23dpa8
    title: Blogs
    collections_field: blogs
    display_amount: 50
    type: collection_list_set
    enabled: true
---
This is the blogs page