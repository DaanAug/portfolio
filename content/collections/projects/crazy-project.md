---
id: 6bace0bd-1cec-4280-a2af-76fd4a1605b4
blueprint: project
title: 'Crazy project'
description: 'This is nothing less than a crazy project. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iste perspiciatis quo vero nemo, recusandae a autem modi cumque omnis, ipsam quam eum.'
featured_image: octavian-dan-b21ty33cqvs-unsplash.jpg
tags:
  - first
  - statamic
updated_by: 1c59a747-49b0-40d8-9738-c0f9fe1e30dd
updated_at: 1678309187
bard_field:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Big boi title'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
  -
    type: paragraph
skills:
  -
    id: lf05gouz
    name: 'C#'
    description: 'I utilized C# programming language to develop a Windows desktop application, a game, and a web application in my project.'
  -
    id: lf05ibwk
    name: Tailwind
    description: 'I incorporated Tailwind CSS framework into my web project, leveraging its utility-first approach to rapidly create responsive and visually appealing designs.'
  -
    id: lf05kwjt
    name: MySQL
    description: 'I integrated MySQL database management system into my project, allowing me to efficiently store, manage, and retrieve data for my application.'
  -
    id: lf05ksa5
    name: Nginx
    description: 'I deployed my web application on an Nginx web server, utilizing its high-performance features and robust configuration options to efficiently handle incoming traffic.'
repository_link: 'https://gitlab.com/DaanAug/portfolio'
---
