module.exports = {
  content: [
    './resources/**/*.antlers.html',
    './resources/**/*.blade.php',
    './resources/**/*.vue',
    './content/**/*.md'
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
  safelist: [
    '/language-(.*)/',
    'hover:text-fuchsia-500',
    'hover:text-yellow-500',
    'hover:text-red-500',
    'hover:text-emerald-500',
    'hover:text-cyan-500',
    'hover:text-violet-500'
  ],
}
