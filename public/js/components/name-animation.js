const classList = [
  "hover:text-fuchsia-500",
  "hover:text-yellow-500",
  "hover:text-red-500",
  "hover:text-emerald-500",
  "hover:text-cyan-500",
  "hover:text-violet-500",
];

const colorList = [
  "#d946ef",
  "#eab308",
  "#ef4444",
  "#10b981",
  "#06b6d4",
  "#8b5cf6",
]

// Create span for each character
const heading = document.getElementById("name");
const textContent = heading.textContent;
heading.textContent = "";

for (let i = 0; i < textContent.length; i++) {
  // Select random color from the list
  const randomNumber = Math.floor(Math.random() * classList.length)
  const randomClass = classList[randomNumber];
  const randomColor = colorList[randomNumber];

  // Apply class and content to the span
  const span = document.createElement("span");
  span.classList.add(randomClass, "transition-colors", "hover-animation");
  span.textContent = textContent[i];

  // Apply startup animation
  span.style.setProperty("--crazy-color", randomColor); // set custom CSS variable for hover color
  span.style.animationDelay = `${i * 0.05 + 0.4 }s`; // set animation delay based on index

  heading.appendChild(span);
}